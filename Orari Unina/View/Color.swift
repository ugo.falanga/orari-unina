//
//  Style.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 27/05/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import SwiftUI

extension Color {
    //MARK: Color of Button
    static let buttonColor = Color("ButtonColor")
    
    //MARK: Color of Text
    static let textColor = Color("TextColor")
}

struct Color_Previews: PreviewProvider {
    static var previews: some View {
        VStack(alignment: .center, spacing: 10) {
            HStack{
                Rectangle().size(CGSize(width: 50, height: 50))
                    .foregroundColor(.buttonColor)
            }
            .padding()
        }
    }
}
