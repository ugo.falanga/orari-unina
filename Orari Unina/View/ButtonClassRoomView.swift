//
//  ButtonClassRoomView.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import SwiftUI

struct ButtonClassroomView: View {
    var className: String
    var dailyTimetable: [HourOfDay]
    
    var body: some View {
        HStack {
            NavigationLink(destination: TimetableView(title: className, timeTable: dailyTimetable), label: {Text(className)
                    .foregroundColor(Color.white)
                    .font(.system(.title, design: .rounded))
                    .fontWeight(.bold)
            })
                .frame(width: 300, height: 70)
                .background(Color.buttonColor)
                .cornerRadius(10)
        }
        .padding([.top, .bottom], 10)
    }
}


struct ButtonClassroomView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonClassroomView(className: "CL-T-1", dailyTimetable: [HourOfDay.init(hour: "8:30", subject: "AM1", occupy: true),
             HourOfDay.init(hour: "9:30", subject: "AM1", occupy: true),
             HourOfDay.init(hour: "10:30", subject: "FG1", occupy: true),
             HourOfDay.init(hour: "11:30", subject: "FG1", occupy: true),
             HourOfDay.init(hour: "12:30", subject: "---", occupy: false),
             HourOfDay.init(hour: "13:30", subject: "---", occupy: false),
             HourOfDay.init(hour: "14:30", subject: "---", occupy: false),
             HourOfDay.init(hour: "15:30", subject: "---", occupy: false),
             HourOfDay.init(hour: "16:30", subject: "---", occupy: false),
             HourOfDay.init(hour: "17:30", subject: "---", occupy: false)
            ])
    }
}
