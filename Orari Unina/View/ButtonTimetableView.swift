//
//  ButtonTimetableView.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import SwiftUI

struct ButtonTimetableView: View {
    //View for timetable
    var hour: String
    var subject: String
    
    var body: some View{
        HStack{
            Text(hour)
                .frame(width: 80, height: 25)
                .foregroundColor(Color.white)
                .background(Color.buttonColor)
                .cornerRadius(10)
                .padding(.trailing, 5)
            
            Text(subject)
                .lineLimit(2)
        }
        .frame(height: 50)
    }
}

struct ButtonTimetableView_Previews: PreviewProvider {
    static var previews: some View {
        //        ButtonTimetableView(hour: "17:30", subject: "Analisi Matematica 1")
        ButtonTimetableView(hour: "18:30", subject: "Circuiti di Elaborazione dei Segnali per la Bioingegneria")
    }
}
