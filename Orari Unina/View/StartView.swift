//
//  ContentView.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import SwiftUI

struct StartView: View {
    let days = ["Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì"]
    let hours = ["8:30", "9:30", "10:30", "11:30", "12:30", "13:30", "14:30", "15:30", "16:30", "17:30", "18:30"]
    let locations = ["P.le Tecchio", "V. Claudio", "V. Nuova Agnano", "San Giovanni"]
    @State private var selectedDay = 0
    @State private var selectedHour = 0
    @State private var selectedLocation = 0
    
    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                VStack {
                    Text("Cerca un aula libera...")
                        .font(.system(size: 30, weight: .bold, design: .rounded))
                    HStack {
                        VStack {
                            Text("Giorno")
                                .frame(width: 100, height: 25)
                                .background(Color.buttonColor)
                                .font(.system(.subheadline, design: .rounded))
                                .foregroundColor(Color.white)
                                .cornerRadius(5)
                            Picker(selection: self.$selectedDay, label: Text("Giorno")) {
                                ForEach(0 ..< self.days.count) {
                                    Text(self.days[$0])
                                }
                                .font(.system(.headline, design: .rounded))
                            }
                            .labelsHidden()
                            .pickerStyle(.wheel)
                            .frame(maxWidth: geometry.size.width / 3, maxHeight: geometry.size.height / 7)
                            .clipped()
                        }
                        VStack {
                            Text("Ora")
                                .frame(width: 100, height: 25)
                                .background(Color.buttonColor)
                                .font(.system(.subheadline, design: .rounded))
                                .foregroundColor(Color.white)
                                .cornerRadius(5)
                            Picker(selection: self.$selectedHour, label: Text("Ora")) {
                                ForEach(0 ..< self.hours.count) {
                                    Text(self.hours[$0])
                                }
                                .font(.system(.headline, design: .rounded))
                            }
                            .labelsHidden()
                            .pickerStyle(.wheel)
                            .frame(maxWidth: geometry.size.width / 5, maxHeight: geometry.size.height / 7)
                            .clipped()
                        }
                        VStack {
                            Text("Sede")
                                .frame(width: 100, height: 25)
                                .background(Color.buttonColor)
                                .font(.system(.subheadline, design: .rounded))
                                .foregroundColor(Color.white)
                                .cornerRadius(5)
                            Picker(selection: self.$selectedLocation, label: Text("Plesso")) {
                                ForEach(0 ..< self.locations.count) {
                                    Text(self.locations[$0])
                                }
                                .font(.system(.headline, design: .rounded))
                            }
                            .labelsHidden()
                            .pickerStyle(.wheel)
                            .frame(maxWidth: geometry.size.width / 3, maxHeight: geometry.size.height / 7)
                            .opacity(2)
                            .clipped()
                        }
                    }
                    .padding(.top, 100)
                    if self.locations[self.selectedLocation] == "P.le Tecchio" {
                        NavigationLink(destination: ResultView(building: tecchio, inputDay: self.days[self.selectedDay], inputHour: self.hours[self.selectedHour])){
                            HStack {
                                Image(systemName: "magnifyingglass")
                                    .foregroundColor(Color.white)
                                Text("Cerca...")
                                    .foregroundColor(Color.white)
                            }
                        }
                            .frame(width: 200, height: 75)
                            .background(Color.buttonColor)
                            .cornerRadius(10)
                            .padding(.top, 50)
                    } else if self.locations[self.selectedLocation] == "V. Claudio" {
                        NavigationLink(destination: ResultView(building: claudio, inputDay: self.days[self.selectedDay], inputHour: self.hours[self.selectedHour])){
                            HStack {
                                Image(systemName: "magnifyingglass")
                                    .foregroundColor(Color.white)
                                Text("Cerca...")
                                    .foregroundColor(Color.white)
                            }
                        }
                            .frame(width: 200, height: 75)
                            .background(Color.buttonColor)
                            .cornerRadius(10)
                            .padding(.top, 50)
                    } else if self.locations[self.selectedLocation] == "V. Nuova Agnano" {
                        NavigationLink(destination: ResultView(building: agnano, inputDay: self.days[self.selectedDay], inputHour: self.hours[self.selectedHour])) {
                            HStack {
                                Image(systemName: "magnifyingglass")
                                    .foregroundColor(Color.white)
                                Text("Cerca...")
                                    .foregroundColor(Color.white)
                            }
                        }
                            .frame(width: 200, height: 75)
                            .background(Color.buttonColor)
                            .cornerRadius(10)
                            .padding(.top, 50)
                    } else if self.locations[self.selectedLocation] == "San Giovanni" {
                        NavigationLink(destination: ResultView(building: giovanni, inputDay: self.days[self.selectedDay], inputHour: self.hours[self.selectedHour])){
                            HStack {
                                Image(systemName: "magnifyingglass")
                                    .foregroundColor(Color.white)
                                Text("Cerca...")
                                    .foregroundColor(Color.white)
                            }
                        }
                            .frame(width: 200, height: 75)
                            .background(Color.buttonColor)
                            .cornerRadius(10)
                            .padding(.top, 50)
                    }
                }
            }
            .background(Image("logo").padding(.top, 630).padding(.leading, 328))
        }
        
    }
    
}


struct StartView_Previews: PreviewProvider {
    static var previews: some View {
        StartView()
    }
}
