//
//  ResultView.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import SwiftUI

struct ResultView: View {
    @State var showingAlert: Bool = true
    
    var building: [ClassroomModel]
    var inputDay: String
    let inputHour: String
    
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            Spacer()
            if inputDay == "Lunedì" {
                VStack {
                    ForEach(building, id: \.id) { item in
                        self.checkHour(data: item, day: item.timetable.Monday)
                    }
                }
            } else if inputDay == "Martedì" {
                VStack {
                    ForEach(building, id: \.id) { item in
                        self.checkHour(data: item, day: item.timetable.Tuesday)
                    }
                }
            } else if inputDay == "Mercoledì" {
                VStack {
                    ForEach(building, id: \.id) { item in
                        self.checkHour(data: item, day: item.timetable.Wednesday)
                    }
                }
            } else if inputDay == "Giovedì" {
                VStack {
                    ForEach(building, id: \.id) { item in
                        self.checkHour(data: item, day: item.timetable.Thursday)
                    }
                }
            } else if inputDay == "Venerdì" {
                VStack {
                    ForEach(building, id: \.id) { item in
                        self.checkHour(data: item, day: item.timetable.Friday)
                    }
                }
            }
        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text("Attenzione!"), message: Text("Gli orari potrebbero subire variazioni o non essere corretti al momento della pubblicazione.\nPer qualsiasi problema segnalare eventuali errori allo sviluttatore."), dismissButton: .default(Text("Ok")))
        }
        .navigationBarTitle("Aule")
    }
    
// Ricerca tramite ora.
    func checkHour(data: ClassroomModel, day: [HourOfDay]) -> AnyView {
        if inputHour == "8:30" && day[0].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "9:30" && day[1].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "10:30" && day[2].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "11:30" && day[3].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "12:30" && day[4].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "13:30" && day[5].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "14:30" && day[6].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "15:30" && day[7].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "16:30" && day[8].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "17:30" && day[9].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else if inputHour == "18:30" && day[10].occupy == false {
            return AnyView(ButtonClassroomView(className: data.classroomName, dailyTimetable: day))
        } else {
            return AnyView(ButtonClassroomView(className: "", dailyTimetable: []).frame(width: 0, height: 0))
        }
    }
}


struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        ResultView(building: claudio, inputDay: "Lunedì", inputHour: "8:30")
    }
}
