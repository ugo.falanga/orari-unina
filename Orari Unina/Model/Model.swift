//
//  Model.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import Foundation

// Struct for various hours of the day
struct HourOfDay: Codable, Hashable{
    var hour: String
    var subject: String
    var occupy: Bool
}

// Struct for a timetable of the classroom
struct Timetable: Codable, Hashable{
    var Monday: [HourOfDay]
    var Tuesday: [HourOfDay]
    var Wednesday: [HourOfDay]
    var Thursday: [HourOfDay]
    var Friday: [HourOfDay]
}

// Struct for JSON file
struct ClassroomModel: Identifiable, Codable, Hashable {
    var classroomName: String
    var id: Int
    var timetable:Timetable
}
