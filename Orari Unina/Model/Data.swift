//
//  Data.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import Foundation

// Read the information from Json file
let claudio: [ClassroomModel] = load("OrariClaudio.json")
let tecchio: [ClassroomModel] = load("OrariTecchio.json")
let agnano: [ClassroomModel] = load("OrariAgnano.json")
let giovanni: [ClassroomModel] = load("OrariS_Giovanni.json")

// Function to parse the json file
func load<T: Decodable>(_ filename: String) -> T {
    let data: Data
    
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil) else {
        fatalError("Couldn't find \(filename) in main bundle.")
    }
    do{
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }
    
    do{
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}
