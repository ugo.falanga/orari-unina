//
//  TimetableView.swift
//  Orari Unina
//
//  Created by Ugo Falanga on 22/04/2020.
//  Copyright © 2020 Ugo Falanga. All rights reserved.
//

import SwiftUI

struct TimetableView: View {
    var title: String
    var timeTable: [HourOfDay]
    
    var body: some View {
        VStack {
            List {
                ForEach(timeTable, id: \.self) { time in
                    ButtonTimetableView(hour: time.hour, subject: time.subject)
                }
            }
            .onAppear{
                UITableView.appearance().separatorStyle = .none
                UITableView.appearance().separatorColor = .clear
            }
            HStack {
                Text("[C] -> da controllare \n[Materia] -> Dura mezz'ora")
                    .font(.system(.headline, design: .rounded))
                    .foregroundColor(.textColor)
            }
        }
        .navigationBarTitle(Text("\(title)"), displayMode: .inline)
    }
}


struct TimetableView_Previews: PreviewProvider {
    static var previews: some View {
        TimetableView(title: "CL-T-1", timeTable: [HourOfDay.init(hour: "8:30", subject: "Analisi Matematica 1", occupy: true),
                                                   HourOfDay.init(hour: "9:30", subject: "Analisi Matematica 1", occupy: true),
                                                   HourOfDay.init(hour: "10:30", subject: "Fisica Generale 1", occupy: true),
                                                   HourOfDay.init(hour: "11:30", subject: "Fisica Generale 1", occupy: true),
                                                   HourOfDay.init(hour: "12:30", subject: "---------", occupy: false),
                                                   HourOfDay.init(hour: "13:30", subject: "---------", occupy: false),
                                                   HourOfDay.init(hour: "14:30", subject: "---------", occupy: false),
                                                   HourOfDay.init(hour: "15:30", subject: "---------", occupy: false),
                                                   HourOfDay.init(hour: "16:30", subject: "---------", occupy: false),
                                                   HourOfDay.init(hour: "17:30", subject: "---------", occupy: false),
                                                   HourOfDay.init(hour: "18:30", subject: "---------", occupy: false)])
    }
}
